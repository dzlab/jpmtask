package dz.lab.jpmtask.process;

import dz.lab.jpmtask.AppConfig;
import dz.lab.jpmtask.data.Trade;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import reactor.Environment;
import reactor.bus.EventBus;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by bachir on 19/09/16.
 */
public class ShareIndexProcessorTest {
    private AppConfig config;
    private Environment env;
    private EventBus eventBus;

    @Before
    public void init() {
        config = new AppConfig();
        env = config.env();
        eventBus = config.eventBus(env);
    }
    @After
    public void teardown() {
        env.shutdown();
    }

    /**
     * Test the share index processor
     */
    @Test
    public void testProcess() {
        long window = 200;
        long now = System.currentTimeMillis();

        ShareIndexProcessor processor = new ShareIndexProcessor();

        // processing context
        Context context = new Context(eventBus);

        // add first trade
        Trade trade = new Trade(now - 100, "TEA", BigDecimal.ONE, Trade.Indicator.BUY, 2);
        processor.process(trade, context);

        assertEquals(BigDecimal.ONE, context.getShareIndex());

        // add second trade
        trade = new Trade(now - 70, "POP", BigDecimal.valueOf(9), Trade.Indicator.SELL, 1);
        processor.process(trade, context);

        assertEquals(BigDecimal.valueOf(2.7), context.getShareIndex()); // (1*9)exp(1/(1+1))

        // add a third trade
        trade = new Trade(now - 20, "ALE", BigDecimal.valueOf(5), Trade.Indicator.SELL, 1);
        processor.process(trade, context);

        // add a fourth trade with different symbol
        trade = new Trade(now + 100, "GIN", BigDecimal.valueOf(7), Trade.Indicator.SELL, 1);
        processor.process(trade, context);

        assertTrue(BigDecimal.valueOf(4.210).compareTo(context.getShareIndex())==0); // (1*9*5*7)exp(1/(1+1+1+1))
    }
}
