package dz.lab.jpmtask.process;

import dz.lab.jpmtask.data.GBCE;
import dz.lab.jpmtask.data.JpaGBCERepository;
import dz.lab.jpmtask.data.Trade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 */
public class DividendTradeProcessor implements TradeProcessor {

    private static final Logger logger = LoggerFactory.getLogger(DividendTradeProcessor.class);

    /**
     * JPA repository for accessing GBCE data.
     */
    private JpaGBCERepository repository;

    /**
     * Constructor
     */
    public DividendTradeProcessor(JpaGBCERepository repository) {
        Assert.notNull(repository, "The GBCE repository must not be null");
        this.repository = repository;
    }

    /**
     * Process the {@link Trade} and calculate its dividend.
     * @param trade the trade to process
     * @return
     */
    public boolean process(Trade trade, Context context) {
        logger.debug("handling {}", trade);
        BigDecimal price = trade.getPrice();
        Assert.isTrue(price.compareTo(BigDecimal.ZERO)>0, "Trade price must be positive.");
        String key = trade.getSymbol();
        if(repository.exists(key)==false) {
            // cannot process unknown symbol
            return false;
        }
        GBCE data = repository.findOne(key);
        logger.info("GBCE found for {}: {}", key, data);
        BigDecimal dividend = BigDecimal.ZERO;
        switch (data.getType()) {
            case COMMON:
                dividend = calculateCommonDividend(data.getLastDividend(), price);
                break;
            case PREFERRED:
                dividend = calculatePreferredDividend(data.getFixedDividend(), data.getParValue(), price);
                break;
            default:
                logger.info("Unknown type for {}", trade);
        }
        logger.info("Dividend={}", dividend);
        BigDecimal ratio = calculatePERatio(dividend, price);
        // emit the calculated Dividend and P/E ratio
        context.emitDividend(trade.getSymbol(), dividend);
        context.emitPERatio(trade.getSymbol(), ratio);
        // success
        return true;
    }

    /**
     * Calculate the common dividend.
     * @param lastDividend the Last Dividend.
     * @param price the trade price.
     * @return the calculated dividend or <code>0</code> if this has no common dividend.
     */
    public BigDecimal calculateCommonDividend(BigDecimal lastDividend, BigDecimal price) {
        if(price == BigDecimal.ZERO) {
            // cannot divide by zero
            return null;
        }
        if(lastDividend == BigDecimal.ZERO) {
            return BigDecimal.ZERO;
        }
        return lastDividend.divide(price, 10, RoundingMode.HALF_UP);
    }

    /**
     * Calculate the preferred dividend.
     * @param fixedDividend the Fixed Dividend.
     * @param parValue the Par Value.
     * @param price the trade price.
     * @return the calculated dividend or <code>null</code> if this has no fixed dividend.
     */
    public BigDecimal calculatePreferredDividend(BigDecimal fixedDividend, Integer parValue, BigDecimal price) {
        if(price == BigDecimal.ZERO) {
            // cannot divide by zero
            return null;
        }
        BigDecimal parValueBD = BigDecimal.valueOf(parValue);
        BigDecimal product = fixedDividend.multiply(parValueBD);
        return product.divide(price);
    }

    /**
     * Calculate P/E ratio.
     * @param dividend the dividend
     * @param price the ticker price
     * @return the calculated ratio, or <code>null</code> if the dividend is zero.
     */
    public BigDecimal calculatePERatio(BigDecimal dividend, BigDecimal price) {
        if(dividend == BigDecimal.ZERO) {
            // cannot divide by zero
            return null;
        }
        BigDecimal result = BigDecimal.ZERO;
        try{
            result = price.divide(dividend);
        }catch(Exception e) {
            result = price.divide(dividend, 10, RoundingMode.HALF_UP);
        }
        return result;
    }
}
