package dz.lab.jpmtask;

import dz.lab.jpmtask.data.GBCE;
import dz.lab.jpmtask.data.JpaGBCERepository;
import dz.lab.jpmtask.data.JpaTradeRepository;
import dz.lab.jpmtask.data.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import reactor.Environment;

/**
 * Created by bachir on 03/09/16.
 */
@SpringBootApplication
public class App implements ApplicationRunner, ApplicationListener<ContextClosedEvent>, HealthIndicator {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    @Autowired
    JpaTradeRepository trades;

    /*@Autowired
    JpaGBCERepository gbce;*/

    @Autowired
    ThreadPoolTaskScheduler scheduler;

    @Autowired
    SimpleStockMarket stockMarket;

    public App() {}

    /**
     * Provides information about the app health that will be exposed by an Actuator instance.
     * @return a {@link Health} instance.
     */
    @Override
    public Health health() {
        return Health.up().withDetail("trades", trades.count()).build();
    }

    /**
     * Run the application. ('--name=whatever')
     * @throws Exception on error.
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        /*trades.save(new Trade(1L, "USD", BigDecimal.ONE, Trade.Indicator.BUY, 10));
        for(Trade instance: trades.findBySymbol("USD")) {
            logger.info(instance.toString());
        }
        for(GBCE instance: gbce.findAll()) {
            logger.info(instance.toString());
        }*/
        logger.info( "Please enter stock tickers as follows: TIMESTAMP,SYMBOL,PRICE,BUY/SELL,QUANTITY" );
        scheduler.execute(stockMarket);
    }

    /**
     * TODO find a viable way to use command line args and inject appropriate Source (e.g. file vs STDIN) into appconf
     * @param args the CLI arguments
     * @throws Exception
     */
    public static void main( String[] args ) throws Exception {
        // start the world.
        ApplicationContext app = SpringApplication.run(App.class, args);

        Thread.sleep(60 * 1000);

        // Stop the world.
        app.getBean(Environment.class).shutdown();

        logger.info("Exit(0).");
    }

    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent) {
        scheduler.shutdown();
    }
}
