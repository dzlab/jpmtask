package dz.lab.jpmtask.data;

import dz.lab.jpmtask.data.Trade;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by bachir on 07/09/16.
 */
public interface JpaTradeRepository extends CrudRepository<Trade, Long> {

    /**
     * Find all trades for a given stock.
     * @param symbol the stock symbol.
     * @return the list of {@link Trade}
     */
    public List<Trade> findBySymbol(String symbol);
}
