Super Simple Stock Market
==============
[![Build Status](https://travis-ci.org/dzlab/JPMTask.png)](https://travis-ci.org/dzlab/JPMTask)

This project consists of two sub-project:
#### backend 
A spring-boot application that process Trades and store the output in a database.
#### frontend (WORK IN PROGRESS) 
A spring mvc application that tracks/visualises the output of Trades processing over time.
