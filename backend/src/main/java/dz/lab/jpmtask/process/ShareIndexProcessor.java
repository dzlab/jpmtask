package dz.lab.jpmtask.process;

import dz.lab.jpmtask.data.Trade;
import io.github.miraclefoxx.math.BigDecimalMath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicReference;

/**
 * A {@link TradeProcessor} implementation that calculates the GBCE All Share Index.
 * Created by bachir on 10/09/16.
 */
public class ShareIndexProcessor implements TradeProcessor {

    private static final Logger logger = LoggerFactory.getLogger(ShareIndexProcessor.class);

    /**
     * Lock object for atomically calculating the GBCE All Share Index.
     */
    private final Object mutex;
    /**
     * The product of all trade prices.
     */
    private BigDecimal product;
    /**
     * The count of trades.
     */
    private Long count;

    /**
     * Constructor.
     */
    public ShareIndexProcessor() {
        this.mutex = new Object();
        this.product = BigDecimal.ONE;
        this.count = 0L;
    }

    /**
     * Process a {@link Trade} by calculating the GBCE All Share Index.
     * @param trade the trade to process.
     * @param context the trade context.
     * @return <code>true</code>
     */
    public boolean process(Trade trade, Context context) {
        // update product atomically.
        synchronized (mutex) {
            this.product = this.product.multiply(trade.getPrice());
            this.count++;
        }
        BigDecimal root = BigDecimal.ONE.divide(BigDecimal.valueOf(this.count), 10, BigDecimal.ROUND_HALF_UP);
        BigDecimal index = BigDecimalMath.pow(this.product, root);
        context.emitShareIndex(index);
        return true;
    }
}
