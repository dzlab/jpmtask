package dz.lab.jpmtask;

import dz.lab.jpmtask.data.*;
import dz.lab.jpmtask.fetch.ScannerTradeFetcher;
import dz.lab.jpmtask.fetch.TradeFetcher;
import dz.lab.jpmtask.process.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import reactor.Environment;
import reactor.bus.EventBus;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import static reactor.bus.selector.Selectors.$;

/**
 * Spring configuration.
 * Created by bachir on 03/09/16.
 */
@Configuration
@EnableAutoConfiguration
@EnableScheduling
public class AppConfig {

    @Autowired
    JpaTradeRepository trades;

    @Autowired
    JpaGBCERepository gbce;

    /**
     * Configure source of {@link Trade} instances.
     */
    @Bean
    public TradeFetcher tradeFetcher() {
        Scanner source = new Scanner(System.in);
        TradeFetcher fetcher = new ScannerTradeFetcher(source, ",");
        return fetcher;
    }

    /**
     * Create a {@link ThreadPoolTaskExecutor} instance.
     */
    /*@Bean
    public ThreadPoolTaskExecutor threadPoolTaskExecutor() {
        int cores = Runtime.getRuntime().availableProcessors();
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // set the thread pool size
        executor.setCorePoolSize(cores);
        // set max pool size
        executor.setMaxPoolSize(2 * cores);
        // set task queue size
        executor.setQueueCapacity(100);
        // wait for tasks to complete before shutdown
        executor.setWaitForTasksToCompleteOnShutdown(true);
        //executor.setAwaitTerminationSeconds(10);
        return executor;
    }*/

    /**
     * Create a {@link ThreadPoolTaskExecutor} instance.
     */
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        int cores = Runtime.getRuntime().availableProcessors();
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
        scheduler.setPoolSize(cores);
        scheduler.setWaitForTasksToCompleteOnShutdown(true);
        return scheduler;
    }

    /**
     * Configure a data processing for {@link Trade}.
     */
    @Bean
    public TradeProcessor tradeProcessor(Executor executor, TaskScheduler scheduler) {
        // prepare the processors
        TradeProcessor dividend = AsyncTradeProcessor.wrap( new DividendTradeProcessor(gbce), executor);
        WindowTradeProcessor window = new WindowTradeProcessor(scheduler, 15 * 60 * 1000, 1000);
        TradeProcessor index = AsyncTradeProcessor.wrap( new ShareIndexProcessor(), executor);
        TradeProcessor out = new ChainTradeProcessor(Arrays.asList(dividend, window, index));
        return out;
    }

    /**
     * @return Spring bean for storing {@link Trade} instances.
     */
    @Bean
    public TradeRecorder tradeRecorder(Executor executor) {
        //TradeProcessor store = AsyncTradeProcessor.wrap( new JpaTradeRecorder(trades), executor);
        return new JpaTradeRecorder(trades);
    }

    /**
     *
     * @return a {@link SimpleStockMarket} instance.
     */
    @Bean
    public SimpleStockMarket stockMarket() {
        return new SimpleStockMarket();
    }

    @Bean
    public Environment env() {
        return Environment.initializeIfEmpty().assignErrorJournal();
    }

    @Bean
    public EventBus eventBus(Environment env) {
        return EventBus.create(env, Environment.THREAD_POOL);
    }

    @Bean
    public TradeProcessingResultConsumer tradeProcessingResultConsumer(EventBus eventBus) {
        TradeProcessingResultConsumer consumer = new TradeProcessingResultConsumer();
        eventBus.on($(TradeProcessingResultConsumer.CHANNEL), consumer);
        return consumer;
    }
}
