package dz.lab.jpmtask.data;

import dz.lab.jpmtask.process.TradeProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * A {@link TradeProcessor} implementation that stores trades in a {@link List}.
 *
 */
public class ListTradeRecorder implements TradeRecorder {


    private static final Logger logger = LoggerFactory.getLogger(ListTradeRecorder.class);

    /**
     * A synchronized list that stores {@link Trade} records
     */
    private final List<Trade> records;

    public ListTradeRecorder(){
        records = Collections.synchronizedList(new LinkedList<Trade>());
    }

    /**
     * @throws {@link IllegalArgumentException} if the given trade instance is <code>null</code>.
     */
    public boolean record(Trade trade) {
        Assert.notNull(trade, "The trade to record must not be null");
        logger.debug("Storing {}", trade);
        records.add(trade);
        return true;
    }
}
