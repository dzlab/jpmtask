package dz.lab.jpmtask.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 */
@Entity
public class Trade implements Serializable, Comparable<Trade> {

    private static final Logger logger = LoggerFactory.getLogger(Trade.class);

    public enum Indicator {
        BUY, SELL
    }

    public enum Type {
        COMMON,   // 0
        PREFERRED // 1
    }

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    /**
     * Timestamp
     */
    private long timestamp;

    /**
     * Symbol (e.g. GOOG)
     */
    private String symbol;

    /**
     * Quantity of shares
     */
    private int quantity;

    /**
     * Buy or Sell indicator
     */
    private Indicator indicator;

    /**
     * Trade price
     */
    private BigDecimal price;

    /**
     * A flag indicated weather this trade was processed by a workflow or not.
     */
    private boolean processed;

    /**
     * Default constuctor required by JPA.
     */
    protected Trade() {}

    /**
     *
     * @param timestamp
     * @param symbol
     * @param price
     * @param action
     * @param quantity
     */
    public Trade(long timestamp, String symbol, BigDecimal price, Indicator action, int quantity){
        this.timestamp = timestamp;
        this.symbol = symbol;
        this.quantity = quantity;
        this.indicator = action;
        this.price = price;
        this.processed = false;
    }

    public Long getId() {return id;}

    public long getTimestamp() {
        return timestamp;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getQuantity() {
        return quantity;
    }

    public Indicator getIndicator() {
        return indicator;
    }

    public BigDecimal getPrice() { return price; }

    public Boolean getProcessed() { return processed;}

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Trade trade = (Trade) o;

        if (timestamp != trade.timestamp) return false;
        if (quantity != trade.quantity) return false;
        if (!symbol.equals(trade.symbol)) return false;
        if (indicator != trade.indicator) return false;
        return price.equals(trade.price);

    }

    @Override
    public int hashCode() {
        int result = (int) (timestamp ^ (timestamp >>> 32));
        result = 31 * result + symbol.hashCode();
        result = 31 * result + quantity;
        result = 31 * result + indicator.hashCode();
        result = 31 * result + price.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "timestamp=" + timestamp +
                ", symbol='" + symbol + '\'' +
                ", quantity=" + quantity +
                ", indicator=" + indicator +
                ", price=" + price +
                '}';
    }

    /**
     * Compare this trade to another one based on their timestamp.
     * @param other
     * @return a -1, zero, or a 1 as this {@link Trade} timestamp is less than, equal to, or greater than the specified object.
     */
    @Override
    public int compareTo(Trade other) {
        if(other == null)
            return -1;
        if(this == other)
            return 0;
        long diff = this.getTimestamp() - other.getTimestamp();
        if(diff < 0)
            return -1;
        else if(diff > 0)
            return 1;
        return 0;
    }

    /**
     * Parse a line into a new Trade instance
     * @param line a line (e.g. CSV line)
     * @param delimiter the delimiter to use when splitten the line
     * @return the parsed Trade or <code>null</code> if it fails to parse the line.
     */
    public static Trade parse(String line, String delimiter) {
        String[] data = line.split(delimiter);
        Trade trade = null;
        try {
            trade = new Trade(
                    Long.valueOf(data[0]),      // timestamp
                    data[1],                    // symbol
                    new BigDecimal(data[2]),    // price
                    Indicator.valueOf(data[3]), // action
                    Integer.parseInt(data[4])   // quantity
            );
        }catch(Exception e) {
            // log the exception
            logger.info("Failed to parse trade {}", e.getMessage());
        }

        return trade;
    }
}
