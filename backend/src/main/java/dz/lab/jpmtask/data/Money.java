package dz.lab.jpmtask.data;

import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Money represents price in a given currency.
 * Created by bachir on 10/09/16.
 */
public class Money {

    public final static String DEFAULT_CURRENCY = "GBP";
    /**
     * The currency of this money.
     */
    private final String currency;
    /**
     * the value/amount of this money.
     */
    private final BigDecimal value;

    public Money(BigDecimal value) {
        this(DEFAULT_CURRENCY, value);
    }

    public Money(String currency, BigDecimal value) {
        Assert.isTrue(value.compareTo(BigDecimal.ZERO)<0, "Money value should be positive.");
        this.currency = "GBP"; // by default All number values in pennies
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Money money = (Money) o;

        if (!currency.equals(money.currency)) return false;
        return value.equals(money.value);

    }

    @Override
    public int hashCode() {
        int result = currency.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }
}
