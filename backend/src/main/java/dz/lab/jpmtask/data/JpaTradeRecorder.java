package dz.lab.jpmtask.data;

import dz.lab.jpmtask.process.TradeProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

/**
 * A {@link TradeProcessor} implementation that stores trades in a SQL database.
 *
 */
public class JpaTradeRecorder implements TradeRecorder {

    /**
     * logging facility
     */
    private static final Logger logger = LoggerFactory.getLogger(JpaTradeRecorder.class);

    /**
     * A synchronized list that stores {@link Trade} records
     */
    private final JpaTradeRepository repository;

    /**
     * Constructor of {@link JpaTradeRecorder}.
     * @param repository a {@link JpaTradeRepository} instance
     */
    public JpaTradeRecorder(JpaTradeRepository repository){
        Assert.notNull(repository, "The jdbcTemplate must not be null.");
        this.repository = repository;
    }


    /**
     * Store a trade into database
     * @param trade a {@link Trade} instance
     */
    public boolean record(Trade trade) {
        Assert.notNull(trade, "The trade must not be null.");
        logger.debug("handling "+trade);
        repository.save(trade);
        //return true;
        return repository.exists(trade.getId());
    }
}
