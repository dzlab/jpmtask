package dz.lab.jpmtask.process;

import org.springframework.util.Assert;
import reactor.bus.Event;
import reactor.bus.EventBus;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Trade processing context.
 * Created by bachir on 04/09/16.
 */
public class Context {

    public static final String DIVIDEND = "dividend";
    public static final String PE_RATIO = "pe_ratio";
    public static final String STOCK_PRICE = "stock_price";
    public static final String SHARE_INDEX = "share_index";

    /**
     * Event bus for an asynchronous communication.
     */
    private final EventBus eventBus;

    /**
     * An atomic reference to the All Share Index.
     */
    private final AtomicReference<BigDecimal> shareIndexRef;

    /**
     * Output store for trades processing. It's used as input for next trades.
     */
    private final Map<String, Map<String, Object>> outputBySymbol;

    /**
     * Create an new instance of the context for trades processing.
     */
    public Context(EventBus eventBus) {
        Assert.notNull(eventBus, "The event bus must not be null.");
        this.eventBus = eventBus;
        this.outputBySymbol = new ConcurrentHashMap<String, Map<String, Object>>();
        this.shareIndexRef = new AtomicReference<BigDecimal>(BigDecimal.ZERO);
    }

    /**
     * Add an output value.
     */
    public void addOutput(String symbol, String name, Object value) {
        Map<String, Object> output = this.outputBySymbol.get(symbol);
        if(output==null) {
            output = Collections.synchronizedMap(new HashMap<String, Object>());
            this.outputBySymbol.put(symbol, output);
        }
        output.put(name, value);

        // notify any component register for this update
        if(this.eventBus.getDispatcher().alive()) {
            TradeProcessingResult result = new TradeProcessingResult();
            result.setSymbol(symbol);
            result.setKey(name);
            result.setValue(value);
            this.eventBus.notify(TradeProcessingResultConsumer.CHANNEL, Event.wrap(result));
        }
    }

    /**
     * Emit the latest value for the calculated dividend.
     * @param dividend the dividend value.
     */
    public void emitDividend(String symbol, BigDecimal dividend) {
        addOutput(symbol, DIVIDEND, dividend);
    }

    /**
     * Emit the latest value for the calculated dividend.
     * @param symbol the stock symbol.
     * @param price the stock price.
     */
    public void emitStockPrice(String symbol, BigDecimal price) { addOutput(symbol, STOCK_PRICE, price);}

    /**
     * Get the current Stock Price
     * @return the last calculated Stock Price or zero if nothing was calculated
     */
    public BigDecimal getStockPrice(String symbol) {
        return getBigDecimal(symbol, STOCK_PRICE);
    }

    /**
     * Emit the latest value for the GBCE All Share Index.
     * @param index the calculated share index.
     */
    public void emitShareIndex(BigDecimal index) { this.shareIndexRef.set(index);}

    /**
     *
     * @return The calculated share index.
     */
    public BigDecimal getShareIndex() {
        return shareIndexRef.get();
    }

    /**
     * Get the last calculated Dividend.
     * @return dividend.
     */
    public BigDecimal getLastDividend(String symbol) {
        return getBigDecimal(symbol, DIVIDEND);
    }

    /**
     * Emit the latest value for the calculated P/E ratio.
     * @param ratio the P/E ratio value.
     */
    public void emitPERatio(String symbol, BigDecimal ratio) {
        addOutput(symbol, PE_RATIO, ratio);
    }

    public BigDecimal getPERatio(String symbol) {
        return getBigDecimal(symbol, PE_RATIO);
    }

    /**
     *
     * @param symbol the stock symbol
     * @param key the price key
     * @return the value or <code>BigDecimal.ZERO</code> if key not found.
     */
    BigDecimal getBigDecimal(String symbol, String key) {
        if(this.outputBySymbol.containsKey(symbol)==false) {
            return BigDecimal.ZERO;
        }
        Map<String, Object> output = this.outputBySymbol.get(symbol);
        BigDecimal decimal = (BigDecimal) output.get(key);
        return decimal;
    }
}
