package dz.lab.jpmtask;

import dz.lab.jpmtask.data.Trade;

import java.math.BigDecimal;

import org.junit.Test;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * A collection of tests for {@link Trade}.
 * Created by bachir on 13/12/15.
 */
public class TradeTest {

    /**
     * Test parsing lines into {@link Trade} instances.
     */
    @Test
    public void testParse()
    {
        // prepare test input
        String[] lines = {
                "1,goog,1.2,BUY,1",
                "2 MSFT 1.9 SELL 20",
                "3  10  SELL    1"
        };
        String[] delimiters = {",", " ", "\t"};
        Trade[] result = {
                new Trade(1, "goog", BigDecimal.valueOf(1.2), Trade.Indicator.BUY, 1),
                new Trade(2, "MSFT", BigDecimal.valueOf(1.9), Trade.Indicator.SELL, 20),
                null
        };
        // check result is OK
        for(int i=0; i<lines.length; i++) {
            Trade parsed = Trade.parse(lines[i], delimiters[i]);
            if(result[i] == null) {
                assertNull(parsed);
            } else {
                assertNotNull(parsed);
                assertTrue(result[i].equals(parsed));
            }
        }
    }

    /**
     * Tests for comparing trades.
     */
    @Test
    public void testCompare() {
        Trade t0 = Trade.parse("1,tea,1.2,BUY,1", ",");
        Trade t1 = Trade.parse("5,pop,1.2,BUY,1", ",");
        Trade t2 = Trade.parse("9,pop,1.2,BUY,1", ",");

        assertTrue(t0.compareTo(t0)==0);
        assertTrue(t0.compareTo(t1)<0);
        assertTrue(t0.compareTo(t2)<0);
        assertTrue(t1.compareTo(t0)>0);
        assertTrue(t2.compareTo(t0)>0);
        assertTrue(t1.compareTo(t2)<0);
        assertTrue(t2.compareTo(t1)>0);
    }
}
