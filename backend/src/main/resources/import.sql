

insert into GBCE(symbol, type, last_dividend, par_value) values ('TEA', 0, 0, 100) -- 'Common'
insert into GBCE(symbol, type, last_dividend, par_value) values ('POP', 0, 8, 100) -- 'Common'
insert into GBCE(symbol, type, last_dividend, par_value) values ('ALE', 0, 23, 60) -- 'Common'
insert into GBCE(symbol, type, last_dividend, fixed_dividend, par_value) values ('GIN', 1, 8, 0.02, 100) -- 'Preferred'
insert into GBCE(symbol, type, last_dividend, par_value) values ('JOE', 0, 13, 250) -- 'Common'