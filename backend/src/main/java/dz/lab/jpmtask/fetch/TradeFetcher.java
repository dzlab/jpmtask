package dz.lab.jpmtask.fetch;

import dz.lab.jpmtask.data.Trade;

/**
 * An interface defining a {@link Trade} fetcher.
 * Created by bachir on 03/09/16.
 */
public interface TradeFetcher {
    /**
     * Fetch a {@link Trade} instance from a source.
     * @return next {@link Trade} instance.
     */
    public Trade next();

    /**
     * Close the fetcher.
     */
    public void close();
}
