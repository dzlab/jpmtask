package dz.lab.jpmtask.process;

import dz.lab.jpmtask.data.Trade;

/**
 * An interface defining a {@link Trade} processor.
 */
public interface TradeProcessor {
    /**
     * Process a {@link Trade} instance.
     * @param trade the trade to process
     * @return <code>true</code> if the process succeeded, <code>false</code> otherwise
     */
    public boolean process(Trade trade, Context context);
}
