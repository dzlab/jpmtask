package dz.lab.jpmtask.process;

import dz.lab.jpmtask.AppConfig;
import dz.lab.jpmtask.TestUtils;
import dz.lab.jpmtask.data.Trade;
import dz.lab.jpmtask.process.Context;
import dz.lab.jpmtask.process.WindowTradeProcessor;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.Environment;
import reactor.bus.EventBus;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * A collection of tests for {@link WindowTradeProcessor}.
 * Created by bachir on 14/12/15.
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class WindowTradeProcessorTest {
    private AppConfig config;
    private Environment env;
    private EventBus eventBus;
    private ThreadPoolTaskScheduler scheduler;

    @Before
    public void init() {
        config = new AppConfig();
        env = config.env();
        eventBus = config.eventBus(env);
        scheduler = config.threadPoolTaskScheduler();
        scheduler.initialize();
    }
    @After
    public void teardown() {
        env.shutdown();
        scheduler.shutdown();
    }

    /**
     * Test the window processor
     */
    @Test
    public void testProcess() {
        long window = 200;
        long now = System.currentTimeMillis();
        // create a processor with a window of 1
        WindowTradeProcessor processor = new WindowTradeProcessor(scheduler, window, window/2);

        // processing context
        Context context = new Context(eventBus);

        // add first trade
        Trade trade = new Trade(now - 100, "tea", BigDecimal.ONE, Trade.Indicator.BUY, 2);
        processor.process(trade, context);

        // add second trade
        trade = new Trade(now - 70, "tea", BigDecimal.TEN, Trade.Indicator.SELL, 1);
        processor.process(trade, context);

        // add a third trade with different symbol
        trade = new Trade(now - 20, "pop", BigDecimal.TEN, Trade.Indicator.SELL, 1);
        processor.process(trade, context);

        TestUtils.sleep(10); // time is > now + 10
        assertEquals(BigDecimal.valueOf(4), context.getStockPrice("tea")); // stock price should be (10+2)/(1+2)=4
        assertEquals(BigDecimal.TEN, context.getStockPrice("pop")); // stock price should be (10+2)/(1+2)=4

        // add a forth trade
        trade = new Trade(now + 100, "tea", BigDecimal.valueOf(2.5), Trade.Indicator.SELL, 1);
        processor.process(trade, context);

        TestUtils.sleep(100); // wait a second to be sure the trade is processed
        assertEquals(BigDecimal.valueOf(6.25), context.getStockPrice("tea")); // stock price should be (10+2)/(1+2)=4
        assertEquals(BigDecimal.TEN, context.getStockPrice("pop")); // stock price should be (10+2)/(1+2)=4
    }

    /**
     * Test the result of executing one run of {@link WindowTradeProcessor.ScheduledTask}.
     */
    @Test
    public void testScheduledTask() {
        Context context = new Context(eventBus);
        long window = 200; // a time window of two that slides by 1
        long now = System.currentTimeMillis();
        // prepare trades
        Map<String, Set<Trade>> tradeMap = new ConcurrentHashMap<String, Set<Trade>>();
        Set<Trade> teaTrades = new ConcurrentSkipListSet<Trade>();
        teaTrades.add(new Trade(now - 160, "tea", BigDecimal.ONE, Trade.Indicator.BUY, 2));
        teaTrades.add(new Trade(now - 70, "tea", BigDecimal.TEN, Trade.Indicator.SELL, 1));
        tradeMap.put("tea", teaTrades);
        Set<Trade> popTrades = new ConcurrentSkipListSet<Trade>();
        popTrades.add(new Trade(now - 120, "pop", BigDecimal.TEN, Trade.Indicator.SELL, 1));
        tradeMap.put("pop", popTrades);

        WindowTradeProcessor.ScheduledTask task = new WindowTradeProcessor.ScheduledTask(context, tradeMap, window);

        // simulate a scheduled run and check the processing result
        task.run();
        assertEquals(2, tradeMap.get("tea").size());
        assertEquals(1, tradeMap.get("pop").size());
        assertEquals(BigDecimal.valueOf(4), context.getStockPrice("tea"));    // price should be (10+2)/(1+2)=4
        assertEquals(BigDecimal.TEN, context.getStockPrice("pop"));           // price should be 10/1=10

        // simulate a scheduled run and check the processing result
        teaTrades.add(new Trade(now + 100, "tea", BigDecimal.valueOf(2.5), Trade.Indicator.SELL, 1));
        TestUtils.sleep(50);
        task.run();
        assertEquals(2, tradeMap.get("tea").size());
        assertEquals(1, tradeMap.get("pop").size());
        assertEquals(BigDecimal.valueOf(6.25), context.getStockPrice("tea")); // stock price should be (10+2.5)/(1+1)=6.25
        assertEquals(BigDecimal.TEN, context.getStockPrice("pop"));           // price should not change

        TestUtils.sleep(window);
        // simulate a scheduled run and check the processing result
        task.run();
        assertEquals(1, tradeMap.get("tea").size());
        assertEquals(0, tradeMap.get("pop").size());
        assertEquals(BigDecimal.valueOf(2.5), context.getStockPrice("tea"));    // price should be 2.5/1=2.5
        assertEquals(BigDecimal.ZERO, context.getStockPrice("pop"));           // price should not change
    }

    /**
     * Test how stock price is calculated.
     */
    @Test
    public void testCalculateStockPrice() {
        List<Trade> trades;
        WindowTradeProcessor processor = new WindowTradeProcessor(scheduler, 1, 1);

        // empty trade list
        trades = Arrays.asList();
        assertEquals(BigDecimal.ZERO, processor.calculateStockPrice(trades));

        // simple
        trades = Arrays.asList(
                new Trade(1, "msft", BigDecimal.TEN, Trade.Indicator.BUY, 3)
        );
        assertEquals(BigDecimal.TEN, processor.calculateStockPrice(trades));

        // simple trade list
        trades = Arrays.asList(
                new Trade(1, "goog", BigDecimal.ONE, Trade.Indicator.BUY, 3),
                new Trade(2, "goog", BigDecimal.ONE, Trade.Indicator.SELL, 2)
        );
        assertEquals(BigDecimal.ONE, processor.calculateStockPrice(trades));

    }
}
