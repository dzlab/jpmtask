package dz.lab.jpmtask.process;

import dz.lab.jpmtask.data.Trade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executor;

/**
 * Wrap a {@link TradeProcessor} and invoke it asynchronously to process a {@link Trade}.
 *
 */
public class AsyncTradeProcessor implements TradeProcessor {


    private static final Logger logger = LoggerFactory.getLogger(AsyncTradeProcessor.class);
    /**
     * The wrapped processor to be called asynchronously
     */
    private final TradeProcessor wrapped;
    /**
     * The executor to use when submit jobs
     */
    private final Executor executor;

    /**
     * Constructor.
     * @param wrapped the wrapped {@link TradeProcessor}.
     * @param executor the {@link Executor} to which async tasks will be submitted.
     */
    public AsyncTradeProcessor(TradeProcessor wrapped, Executor executor) {
        this.wrapped = wrapped;
        this.executor = executor;
    }

    /**
     * Process a {@link Trade} asynchronously.
     * @param trade the trade to process
     * @param context the trade context
     * @return <code>true</code>, otherwise <code>false</code>.
     */
    public boolean process(Trade trade, Context context) {
        logger.debug("handling {}", trade);
        executor.execute(new TradeTask(trade, wrapped, context));
        return true;
    }

    /**
     * Factory method for creating a wrapped {@link TradeProcessor}.
     * @param processor the {@link TradeProcessor}.
     * @param executor the {@link Executor} to which async tasks will be submitted.
     * @return an new instance of {@link AsyncTradeProcessor}.
     */
    public static AsyncTradeProcessor wrap(TradeProcessor processor, Executor executor) {
        return new AsyncTradeProcessor(processor, executor);
    }
}
