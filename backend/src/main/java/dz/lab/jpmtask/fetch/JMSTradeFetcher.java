package dz.lab.jpmtask.fetch;

import dz.lab.jpmtask.data.Trade;

/**
 * An implementation of {@link TradeFetcher} that fetch trades from a JMS queue.
 * This helps distributing the load across many application instances.
 * Created by bachir on 04/09/16.
 */
public class JMSTradeFetcher implements TradeFetcher {

    public JMSTradeFetcher() {}

    /**
     * Fetch a {@link Trade} instance from a JMS queue.
     * @return next {@link Trade} instance.
     */
    public Trade next() {
        return null;
    }

    public void close() {}
}
