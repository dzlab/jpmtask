package dz.lab.jpmtask.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Table of GBCE (Global Beverage Corporation Exchange) data.
 * Created by bachir on 09/09/16.
 */
@Entity
public class GBCE {

    /**
     * Stock symbol
     */
    @Id
    private String symbol;
    /**
     * Stock type
     */
    private Trade.Type type;
    /**
     * Last dividend
     */
    private BigDecimal lastDividend;
    /**
     * Fixed dividend in percentage.
     */
    private BigDecimal fixedDividend;
    /**
     * Par Value.
     */
    private Integer parValue;

    protected GBCE() {}

    public String getSymbol() {
        return symbol;
    }

    public Trade.Type getType() {
        return type;
    }

    public BigDecimal getLastDividend() {
        return lastDividend;
    }

    public BigDecimal getFixedDividend() {
        return fixedDividend;
    }

    public Integer getParValue() {
        return parValue;
    }

    @Override
    public String toString() {
        return "GBCE{" +
                "symbol='" + symbol + '\'' +
                ", type='" + type + '\'' +
                ", lastDividend=" + lastDividend +
                ", fixedDividend=" + fixedDividend +
                ", parValue=" + parValue +
                '}';
    }

    /**
     * Factory method for creating and initialising {@link GBCE} instances.
     * @return
     */
    public static GBCE create(String symbol, String type, BigDecimal lastDividend, BigDecimal fixedDividend, Integer parValue) {
        GBCE result = new GBCE();
        result.symbol = symbol;
        result.type = Trade.Type.valueOf(type);
        result.lastDividend = lastDividend;
        result.fixedDividend = fixedDividend;
        result.parValue = parValue;
        return result;
    }
}
