Super Simple Stock Market
==============

### Run application
```
$ mvn clean package
$ java -jar target/JPMTask-1.0-SNAPSHOT.jar
```
Once the application is running, the following actuator's endpoints are enabled (more can be found in - [link](http://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html)):

* `/health` endpoint on [http://localhost:9001/health](http://localhost:9001/health)
* `/metrics` endpoint on [http://localhost:9001/metrics](http://localhost:9001/metrics)
* `/heapdump` endpoint on [http://localhost:9001/heapdump](http://localhost:9001/heapdump) (to download compressed hprof files).


### Tests
Run all tests
```
$ mvn test
```
Generate coverage reports that will be accessible through ./target/site/jacoco/index.html
```
$ mvn jacoco:report
```

### Todo
Possible improvements:

* Speedup app boostrapping by importing only necessary spring boot auto-config.
* Docker-ise the app and depending services (i.e. DB).
* More runtime metrics to expose through Spring Actuator.
* Use Async annotation for traderecorders instead of manual wrapping.
* Use a distributed queue as source of trades to be able to distributed the load on many instances.
* CLI argument parsing (-f filename: file containing trades, -d delimiter: delimiter to use when parsing lines).