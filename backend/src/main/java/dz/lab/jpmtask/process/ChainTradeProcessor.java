package dz.lab.jpmtask.process;

import dz.lab.jpmtask.data.Trade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Wrap a chain of {@link TradeProcessor} and iterate over them
 *
 */
public class ChainTradeProcessor implements TradeProcessor {

    private static final Logger logger = LoggerFactory.getLogger(ChainTradeProcessor.class);

    /**
     * A list of {@link TradeProcessor}
     */
    private final List<TradeProcessor> processors;

    /**
     * Default constructor.
     * @throws {@link IllegalArgumentException} if the processors is <code>null</code> or empty.
     */
    public ChainTradeProcessor(List<TradeProcessor> processors) {
        Assert.notEmpty(processors, "The list of processors must not be empty.");
        this.processors = processors;
    }

    public boolean process(Trade trade, Context context) {
        logger.debug("handling {}", trade);
        boolean success = true;
        for(TradeProcessor processor: processors) {
            success &= processor.process(trade, context);
        }
        trade.setProcessed(success);
        return success;
    }
}
