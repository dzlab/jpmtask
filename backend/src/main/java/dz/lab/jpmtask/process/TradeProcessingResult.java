package dz.lab.jpmtask.process;

/**
 * A POJO representing the result of processing a {@link Trade}.
 * Created by bachir on 13/09/16.
 */
public class TradeProcessingResult {

    private String symbol;
    private String key;
    private Object value;

    public String getSymbol() {
        return symbol;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TradeProcessingResult{" +
                "symbol='" + symbol + '\'' +
                ", key='" + key + '\'' +
                ", value=" + value +
                '}';
    }
}
