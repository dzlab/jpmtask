package dz.lab.jpmtask;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;

@Controller
public class TradeController {

    @RequestMapping("/trade")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        model.addAttribute("time", Arrays.asList("January","February","March","April","May","June"));
        model.addAttribute("trades", Arrays.asList(203,156,99,251,305,247));
        return "trade";
    }

}
