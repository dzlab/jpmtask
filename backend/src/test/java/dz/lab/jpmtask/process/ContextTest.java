package dz.lab.jpmtask.process;

import dz.lab.jpmtask.AppConfig;
import org.junit.*;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.Environment;
import reactor.bus.Event;
import reactor.bus.EventBus;
import reactor.fn.Consumer;

import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static reactor.bus.selector.Selectors.$;

/**
 * Tests for trades processing {@link Context}.
 * Created by bachir on 13/09/16.
 */
public class ContextTest {

    private AppConfig config;
    private Environment env;
    private EventBus eventBus;

    @Before
    public void init() {
        config = new AppConfig();
        env = config.env();
        eventBus = config.eventBus(env);
    }
    @After
    public void teardown() {
        env.shutdown();
    }

    /**
     * Tests for async event emitting and reception.
     */
    @Test
    public void testConsumerNotify() throws InterruptedException {
        if(!eventBus.getDispatcher().alive()) {
            fail("EventBus internal dispatcher isn't alive.");
        }
        CountDownLatch latch = new CountDownLatch(1);
        SimpleResultConsumer consumer = new SimpleResultConsumer(latch);
        eventBus.on($(TradeProcessingResultConsumer.CHANNEL), consumer);
        Context context = new Context(eventBus);
        context.emitDividend("TEA", BigDecimal.ONE);

        latch.await(5, TimeUnit.SECONDS);
        if(latch.getCount()>0) {
            fail("Timeout while waiting for consumer to receive a notification");
        }
    }

    /**
     * A Simple implementation of {@link Consumer} for testing.
     */
    private static class SimpleResultConsumer implements Consumer<Event<TradeProcessingResult>> {

        private CountDownLatch latch;

        public SimpleResultConsumer(CountDownLatch latch) {
            this.latch = latch;
        }
        @Override
        public void accept(Event<TradeProcessingResult> ev) {
            System.out.println(this+" successfully received event.");
            latch.countDown();
        }
    }
    /**
     * Test for emitting dividends.
     */
    @Test
    public void testEmitDividend() {
        Context context = new Context(eventBus);

        context.emitDividend("goog", BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, context.getLastDividend("goog"));

        context.emitDividend("msft", BigDecimal.valueOf(2.5));
        assertEquals(BigDecimal.valueOf(2.5), context.getLastDividend("msft"));

        context.emitDividend("goog", BigDecimal.ONE);
        assertEquals(BigDecimal.ONE, context.getLastDividend("goog"));
    }

    /**
     * Test for emitting stock price.
     */
    @Test
    public void testEmitStockPrice() {
        Context context = new Context(eventBus);

        context.emitStockPrice("goog", BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, context.getStockPrice("goog"));

        context.emitStockPrice("msft", BigDecimal.valueOf(2.5));
        assertEquals(BigDecimal.valueOf(2.5), context.getStockPrice("msft"));

        context.emitStockPrice("goog", BigDecimal.ONE);
        assertEquals(BigDecimal.ONE, context.getStockPrice("goog"));
    }


    /**
     * Test for emitting stock price.
     */
    @Test
    public void testEmitPERatio() {
        Context context = new Context(eventBus);

        context.emitPERatio("goog", BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, context.getPERatio("goog"));

        context.emitPERatio("msft", BigDecimal.valueOf(2.5));
        assertEquals(BigDecimal.valueOf(2.5), context.getPERatio("msft"));

        context.emitPERatio("goog", BigDecimal.ONE);
        assertEquals(BigDecimal.ONE, context.getPERatio("goog"));
    }

    /**
     * Test for emitting stock price.
     */
    @Test
    public void testEmitShareIndex() {
        Context context = new Context(eventBus);

        context.emitShareIndex(BigDecimal.TEN);
        assertEquals(BigDecimal.TEN, context.getShareIndex());

        context.emitShareIndex(BigDecimal.valueOf(2.5));
        assertEquals(BigDecimal.valueOf(2.5), context.getShareIndex());

        context.emitShareIndex(BigDecimal.ONE);
        assertEquals(BigDecimal.ONE, context.getShareIndex());
    }


}
