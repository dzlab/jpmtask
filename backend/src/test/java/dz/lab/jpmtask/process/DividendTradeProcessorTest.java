package dz.lab.jpmtask.process;

import dz.lab.jpmtask.AppConfig;
import dz.lab.jpmtask.data.GBCE;
import dz.lab.jpmtask.data.JpaGBCERepository;
import dz.lab.jpmtask.data.Trade;
import dz.lab.jpmtask.process.DividendTradeProcessor;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.Environment;
import reactor.bus.EventBus;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
/**
 * A collection of tests for {@link DividendTradeProcessor}
 */
public class DividendTradeProcessorTest {

    private AppConfig config;
    private Environment env;
    private EventBus eventBus;
    private JpaGBCERepository gbce;

    @Before
    public void init() {
        config = new AppConfig();
        env = config.env();
        eventBus = config.eventBus(env);
        // mock GBCE repository
        gbce = Mockito.mock(JpaGBCERepository.class);
        when(gbce.findOne("TEA")).thenReturn(GBCE.create("TEA", "COMMON", BigDecimal.ZERO, BigDecimal.ZERO, 100));
        when(gbce.findOne("POP")).thenReturn(GBCE.create("POP", "COMMON", BigDecimal.valueOf(8), BigDecimal.ZERO, 100));
        when(gbce.findOne("ALE")).thenReturn(GBCE.create("ALE", "COMMON", BigDecimal.valueOf(23), BigDecimal.ZERO, 60));
        when(gbce.findOne("GIN")).thenReturn(GBCE.create("GIN", "PREFERRED", BigDecimal.valueOf(8), BigDecimal.valueOf(0.02), 100));
        when(gbce.findOne("JOE")).thenReturn(GBCE.create("JOE", "COMMON", BigDecimal.valueOf(13), BigDecimal.ZERO ,250));
        when(gbce.exists(anyString())).thenReturn(true);
    }

    @After
    public void teardown() {
        env.shutdown();
    }

    /**
     * Tests for {@link DividendTradeProcessor#process(Trade, Context)}
     */
    @Test
    public void testProcess() {
        long now = System.currentTimeMillis();
        DividendTradeProcessor processor = new DividendTradeProcessor(gbce);
        // processing context
        Context context = new Context(eventBus);

        // add first trade
        Trade trade = new Trade(now - 100, "TEA", BigDecimal.ONE, Trade.Indicator.BUY, 2);
        processor.process(trade, context);

        assertEquals(BigDecimal.ZERO, context.getLastDividend("TEA"));
        assertEquals(null, context.getPERatio("TEA"));

        // add second trade
        trade = new Trade(now - 70, "POP", BigDecimal.TEN, Trade.Indicator.SELL, 1);
        processor.process(trade, context);

        assertTrue(context.getLastDividend("POP").compareTo(BigDecimal.valueOf(0.8))==0);
        assertTrue(context.getPERatio("POP").compareTo(BigDecimal.valueOf(12.5))==0);

        System.out.println("testProcess - "+context.getLastDividend("TEA"));
        // add a third trade
        trade = new Trade(now - 20, "TEA", BigDecimal.valueOf(2.5), Trade.Indicator.SELL, 1);
        processor.process(trade, context);

        // add a fourth trade with different symbol
        trade = new Trade(now + 100, "POP", BigDecimal.valueOf(7), Trade.Indicator.SELL, 1);
        processor.process(trade, context);

        // add a fifth
        trade = new Trade(now + 170, "GIN", BigDecimal.TEN, Trade.Indicator.BUY, 3);
        processor.process(trade, context);

        assertTrue(context.getLastDividend("GIN").compareTo(BigDecimal.valueOf(0.20))==0);
        assertTrue(context.getPERatio("GIN").compareTo(BigDecimal.valueOf(50))==0);
    }

    /**
     * Test for common dividend calculator
     */
    @Test
    public void testCommonDividendCalculator() {
        // input data
        BigDecimal[] lastDividend = {BigDecimal.ZERO, BigDecimal.valueOf(8), BigDecimal.valueOf(13)};
        BigDecimal[] price = {BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE};

        // expected dividend and P/E ration
        BigDecimal[] expectedDividend = {BigDecimal.ZERO, BigDecimal.valueOf(8), BigDecimal.valueOf(13)};
        BigDecimal[] expectedPERatio = {null, BigDecimal.valueOf(0.125), BigDecimal.valueOf(0.0769230769)};

        DividendTradeProcessor processor = new DividendTradeProcessor(gbce);

        // loop over input data and check the output
        for(int i = 0; i<lastDividend.length; i++) {
            BigDecimal dividend = processor.calculateCommonDividend(lastDividend[i], price[i]);
            BigDecimal peratio = processor.calculatePERatio(dividend, BigDecimal.ONE);
            assertTrue(dividend.compareTo(expectedDividend[i])==0);
            if(expectedPERatio[i] == null) {
                assertNull(peratio);
            }else {
                assertTrue(peratio.compareTo(expectedPERatio[i])==0);
            }
        }
    }

    /**
     * Test for preferred dividend calculator
     */
    @Test
    public void testPreferredDividendCalculator() {
        // input data
        BigDecimal[] fixed = {BigDecimal.ZERO, BigDecimal.ONE, BigDecimal.valueOf(0.02)};
        int[] parvalue = {0, 100, 60};
        BigDecimal[] price = {BigDecimal.ONE, BigDecimal.ONE, BigDecimal.ONE};

        // expected dividend and P/E ration
        BigDecimal[] expectedDividend = {BigDecimal.ZERO, BigDecimal.valueOf(100), BigDecimal.valueOf(120, 2)};
        BigDecimal[] expectedPERatio = {null, BigDecimal.valueOf(0.01), BigDecimal.valueOf(0.8333333333)};

        DividendTradeProcessor processor = new DividendTradeProcessor(gbce);

        // loop over input data and check the output
        for(int i = 0; i<fixed.length; i++) {
            BigDecimal dividend = processor.calculatePreferredDividend(fixed[i], parvalue[i], price[i]);
            BigDecimal peratio = processor.calculatePERatio(dividend, BigDecimal.ONE);
            assertEquals(expectedDividend[i], dividend);
            if(expectedPERatio[i] == null) {
                assertNull(peratio);
            }else {
                assertEquals(expectedPERatio[i], peratio);
            }
        }
    }

}
