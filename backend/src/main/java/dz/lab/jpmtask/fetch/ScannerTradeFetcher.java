package dz.lab.jpmtask.fetch;

import dz.lab.jpmtask.data.Trade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.Scanner;

/**
 * An implementation of {@link TradeFetcher} that reads its input from a {@link Scanner}.
 * Created by bachir on 03/09/16.
 */
public class ScannerTradeFetcher implements TradeFetcher {

    private static final Logger logger = LoggerFactory.getLogger(ScannerTradeFetcher.class);
    /**
     * The source of trades to parse.
     */
    private Scanner source;
    /**
     * The delimiter to use when parsing trades.
     */
    private String delimiter = ",";

    /**
     *
     * @param source the trades source.
     * @param delimiter the delimiter to use to split input line.
     * @throws {@link IllegalArgumentException} if any of the paramters is <code>null</code>.
     */
    public ScannerTradeFetcher(Scanner source, String delimiter) {
        Assert.notNull(source, "The trades source must not be null.");
        Assert.notNull(delimiter, "The delimiter must not be null.");
        this.source = source;
        this.delimiter = delimiter;
    }

    /**
     * Fetch a {@link Trade} instance from STDIN.
     * @return next {@link Trade} instance.
     */
    public Trade next(){
        if(source.hasNext()==false) {
            return null;
        }
        // parse a line into a Trade
        String line = source.nextLine();
        logger.debug("Handling: " + line);
        Trade trade = Trade.parse(line, delimiter);
        return trade;
    }

    public void close() {
        source.close();
    }
}
