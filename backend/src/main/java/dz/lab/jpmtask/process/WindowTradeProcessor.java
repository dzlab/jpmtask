package dz.lab.jpmtask.process;

import dz.lab.jpmtask.data.Trade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Process trades within a last window time that slides.
 */
public class WindowTradeProcessor implements TradeProcessor {

    private static final Logger logger = LoggerFactory.getLogger(WindowTradeProcessor.class);

    /**
     * The scheduler
     */
    private final TaskScheduler scheduler;
    /**
     * Window time in milliseconds
     */
    private final long window;

    /**
     * Sliding time in milliseconds
     */
    private final long slide;

    /**
     * A {@link Map} of a {@link List} of {@link Trade} by trade symbol
     */
    private final Map<String, Set<Trade>> tradesBySymbol;

    private AtomicBoolean scheduled;

    /**
     * Default constructor.
     * @param window the window time for trades timestamp
     * @param slide the sliding time for recalculation scheduling
     */
    public WindowTradeProcessor(TaskScheduler scheduler, long window, long slide) {
        Assert.isTrue(window>0, "Time window must be positive integer.");
        Assert.isTrue(slide>0, "Time slide must be positive integer.");
        Assert.notNull(scheduler, "TaskScheduler must not be null.");
        this.window = window;
        this.slide = slide;
        this.scheduler = scheduler;
        this.scheduled = new AtomicBoolean(false);
        this.tradesBySymbol = new ConcurrentHashMap<String, Set<Trade>>();
    }

    /**
     * Process a {@link Trade} by calculating the trade window for the corresponding stock.
     * @param trade the trade to process.
     * @param context the trade context.
     * @return <code>true</code> if the processing succeeded, <code>false</code> otherwise.
     */
    public boolean process(Trade trade, Context context) {
        logger.debug("handling {}", trade);
        Set<Trade> trades = tradesBySymbol.get(trade.getSymbol());
        // if this is the first trade then store it and return
        if(trades == null) {
            logger.debug("Creating a list to store trades with symbol '{}'", trade.getSymbol());
            trades = new ConcurrentSkipListSet<Trade>();
            tradesBySymbol.put(trade.getSymbol(), trades);
        }
        trades.add(trade);
        // schedule the window processing
        if(!scheduled.get()) {
            ScheduledTask task = new ScheduledTask(context, tradesBySymbol, window);
            scheduler.scheduleWithFixedDelay(task, this.slide);
            scheduled.set(true);
        }
        return true;
    }

    /**
     * A scheduled task for calculating a windowed stock-price.
     */
    static class ScheduledTask implements Runnable {

        private static final Logger logger = LoggerFactory.getLogger(ScheduledTask.class);

        /**
         * Processing context
         */
        private final Context context;
        /**
         * Window time in milliseconds
         */
        private final long window;
        /**
         * A {@link Map} of a {@link List} of {@link Trade} by trade symbol
         */
        private final Map<String, Set<Trade>> tradesBySymbol;

        public ScheduledTask(Context context, Map<String, Set<Trade>> tradesBySymbol, long window) {
            this.window = window;
            this.tradesBySymbol = tradesBySymbol;
            this.context = context;
        }

        @Override
        public void run() {
            for(String symbol: this.tradesBySymbol.keySet()) {
                Set<Trade> unfilteredTrades = this.tradesBySymbol.get(symbol);
                if(unfilteredTrades.isEmpty()) {
                    continue;
                }
                // check Trades with timestamp greater than (now - window)
                Long reference = System.currentTimeMillis() - this.window;
                List<Trade> trades = new ArrayList<Trade>();
                for(Trade trade: unfilteredTrades) {
                    if(trade.getTimestamp() > reference) {
                        trades.add(trade);
                    }
                }
                // emit stock price for this symbol
                BigDecimal price = calculateStockPrice(trades);
                context.emitStockPrice(symbol, price);
                // update the list of trades
                removeTrades(unfilteredTrades, reference);
            }
        }

        /**
         * Remove all trades from the head of the trades list whose timestamp is equal to the timestamp in parameter.
         * @param trades the {@link Trade} list to clean.
         * @param timestamp the timestamp of the first trade to remove.
         */
        private void removeTrades(Set<Trade> trades, Long timestamp) {
            for(Trade trade: trades) {
                if(trade.getTimestamp()>timestamp) {
                    break;
                }
                trades.remove(trade);
            }
        }
    }

    /**
     * Calculate the Stock price
     * @param trades list of {@link Trade} instances.
     * @return the stock price.
     */
    static BigDecimal calculateStockPrice(List<Trade> trades) {
        Assert.notNull(trades, "Trade list must not be null.");
        if(trades.isEmpty()) {
            return BigDecimal.ZERO;
        }
        BigDecimal sum1 = BigDecimal.ZERO;
        BigDecimal sum2 = BigDecimal.ZERO;
        for(Trade trade: trades) {
            BigDecimal quantity = new BigDecimal(trade.getQuantity());
            sum1 = sum1.add(trade.getPrice().multiply(quantity));
            sum2 = sum2.add(quantity);
        }
        return sum1.divide(sum2);
    }
}
