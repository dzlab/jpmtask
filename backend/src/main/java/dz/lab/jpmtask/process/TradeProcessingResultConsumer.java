package dz.lab.jpmtask.process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.bus.Event;
import reactor.fn.Consumer;

/**
 * Sample at https://spring.io/guides/gs/messaging-reactor/
 * Created by bachir on 12/09/16.
 */
public class TradeProcessingResultConsumer implements  Consumer<Event<TradeProcessingResult>> {

    private static final Logger logger = LoggerFactory.getLogger(TradeProcessingResultConsumer.class);

    public static final String CHANNEL = "trade_processing_result";

    @Override
    public void accept(Event<TradeProcessingResult> ev) {
        logger.info("Received: {}", ev.getData());
    }
}
