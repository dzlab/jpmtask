package dz.lab.jpmtask.process;

import dz.lab.jpmtask.data.Trade;
import org.springframework.util.Assert;

/**
 * A task that consists of processing a {@link Trade}
 *
 */
public class TradeTask implements Runnable {

    /**
     * the trade
     */
    private final Trade trade;
    /**
     * the processor
     */
    private final TradeProcessor processor;

    private final Context context;

    /**
     *
     * @param trade
     * @param processor
     * @param context
     */
    public TradeTask(Trade trade, TradeProcessor processor, Context context) {
        Assert.notNull(trade, "Trade must not be null.");
        Assert.notNull(processor, "Processor must not be null.");
        Assert.notNull(context, "Context of trade processing must not be null.");
        this.trade = trade;
        this.processor = processor;
        this.context = context;
    }

    /**
     * Run this task and process the trade
     */
    public void run() {
        processor.process(trade, context);
    }
}
