package dz.lab.jpmtask;

import dz.lab.jpmtask.data.Trade;
import dz.lab.jpmtask.data.TradeRecorder;
import dz.lab.jpmtask.fetch.TradeFetcher;
import dz.lab.jpmtask.process.Context;
import dz.lab.jpmtask.process.TradeProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.Lifecycle;
import org.springframework.context.annotation.Configuration;
import reactor.bus.EventBus;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Super Simple Stocks App
 *
 */
public class SimpleStockMarket implements Runnable, Lifecycle
{

    private static final Logger logger = LoggerFactory.getLogger(SimpleStockMarket.class);

    @Autowired
    EventBus eventBus;

    @Autowired
    TradeFetcher in;

    @Autowired
    private TradeProcessor processor;

    @Autowired
    private TradeRecorder out;

    /**
     * A flag for stopping
     */
    private volatile boolean stop;

    public SimpleStockMarket() {
        stop = false;
    }

    /**
     *
     *
     */
    public void run() {
        Context context = new Context(eventBus);
        while (!stop) {
            // get a trade
            Trade trade = in.next();
            // skip failed parse trades
            if(trade == null){
                continue;
            }
            // process
            processor.process(trade, context);
            // store the trade
            out.record(trade);
        }
        logger.info("Stock Market stopped.");
    }

    public void destroy() throws Exception {

    }

    @Override
    public void start() {
        stop = false;
    }

    @Override
    public void stop() {
        stop = true;
        in.close();
    }

    @Override
    public boolean isRunning() {
        return !stop;
    }
}
