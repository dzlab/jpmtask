package dz.lab.jpmtask.data;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by bachir on 09/09/16.
 */
public interface JpaGBCERepository extends CrudRepository<GBCE, String> {

    public List<GBCE> findBySymbol(String symbol);

}
