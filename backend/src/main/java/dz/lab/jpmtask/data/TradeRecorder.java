package dz.lab.jpmtask.data;

/**
 * Created by bachir on 04/09/16.
 */
public interface TradeRecorder {

    public boolean record(Trade trade);
}
